import LinearAlgebra, SparseArrays

# Coldren's book Chapter 7, Section 7.4, Page 427
#TODO: Get boundary conditions right (page 429)
#TODO: Make function for getting reasonable values of n_r
#TODO: Fill out the docs with something useful
"""
    Amat(nl, nlp, neff, DeltaX, DeltaZ)

Create the `A` matrix.

# Arguments
- `nl::AbstractVector`: 
- `nlp::AbstractVector`: 
- `neff::Number`: 
- `DeltaX::Number`: 
- `DeltaZ::Number`: 
"""
function Amat(
    nl::AbstractVector,
    nlp::AbstractVector,
    neff::Number,
    DeltaX::Number,
    DeltaZ::Number,
)
    m = length(nl)
    b = 1 / (2 * DeltaX^2)
    a =
        2 * im * neff / DeltaZ .+ 1 / DeltaX^2 .-
        1 / 2 * ((nlp .^ 2 + nl .^ 2) ./ 2 .- neff^2)
    LinearAlgebra.SymTridiagonal(a, Complex.(fill(-b, m - 1)))
end

"""
    Cmat(nl, nlp, neff, DeltaX, DeltaZ)

Create the `C` matrix.

# Arguments
- `nl::AbstractVector`: 
- `nlp::AbstractVector`: 
- `neff::Number`: 
- `DeltaX::Number`: 
- `DeltaZ::Number`: 
"""
function Cmat(
    nl::AbstractVector,
    nlp::AbstractVector,
    neff::Number,
    DeltaX::Number,
    DeltaZ::Number,
)
    m = length(nl)
    b = 1 / (2 * DeltaX^2)
    a =
        2 * im * neff / DeltaZ .+ 1 / DeltaX^2 .-
        1 / 2 * ((nlp .^ 2 + nl .^ 2) ./ 2 .- neff^2)
    c = -a .+ 4 * im * neff / DeltaZ
    LinearAlgebra.SymTridiagonal(c, complex.(fill(b, m - 1)))
end

"""
    Amat(nl, nlp, neff, DeltaX, Deltay, DeltaZ)

Create the `A` matrix.

# Arguments
- `nl::AbstractMatrix`: 
- `nlp::AbstractMatrix`: 
- `neff::Number`: 
- `DeltaX::Number`: 
- `Deltay::Number`: 
- `DeltaZ::Number`: 
"""
function Amat(
    nl::AbstractMatrix,
    nlp::AbstractMatrix,
    neff::Number,
    DeltaX::Number,
    DeltaY::Number,
    DeltaZ::Number,
)
    m, n = size(nl)
    mn = m * n
    b = 1 / (2 * DeltaY^2) * (1 .- iszero.((1:mn-1) .% m))
    a =
        2 * im * neff / DeltaZ .+ 1 / DeltaX^2 .+ 1 / DeltaY^2 .-
        1 / 2 * ((nlp .^ 2 + nl .^ 2) ./ 2 .- neff^2)
    B = 1 / (2 * DeltaX^2)
    SparseArrays.sparse(
        [1:mn; 1:mn-1; 2:mn; 1:mn-m; m+1:mn],
        [1:mn; 2:mn; 1:mn-1; m+1:mn; 1:mn-m],
        [a[:]; -b; -b; fill(-B, 2 * (n - 1) * m)],
    )
end

"""
    Cmat(nl, nlp, neff, DeltaX, Deltay, DeltaZ)

Create the `C` matrix.

# Arguments
- `nl::AbstractMatrix`: 
- `nlp::AbstractMatrix`: 
- `neff::Number`: 
- `DeltaX::Number`: 
- `Deltay::Number`: 
- `DeltaZ::Number`: 
"""
function Cmat(
    nl::AbstractMatrix,
    nlp::AbstractMatrix,
    neff::Number,
    DeltaX::Number,
    DeltaY::Number,
    DeltaZ::Number,
)
    m, n = size(nl)
    mn = m * n
    b = 1 / (2 * DeltaY^2) * (1 .- iszero.((1:mn-1) .% m))
    a =
        2 * im * neff / DeltaZ .+ 1 / DeltaX^2 .+ 1 / DeltaY^2 .-
        1 / 2 * ((nlp .^ 2 + nl .^ 2) ./ 2 .- neff^2)
    c = -a .+ 4 * im * neff / DeltaZ
    B = 1 / (2 * DeltaX^2)
    SparseArrays.sparse(
        [1:mn; 1:mn-1; 2:mn; 1:mn-m; m+1:mn],
        [1:mn; 2:mn; 1:mn-1; m+1:mn; 1:mn-m],
        [c[:]; b; b; fill(B, 2 * (n - 1) * m)],
    )
end

"""
    philp(phil, nl, nlp, neff, DeltaX, Deltay, DeltaZ)

TODO: what is this

# Arguments
- `phil::AbstractMatrix`: 
- `nl::AbstractMatrix`: 
- `nlp::AbstractMatrix`: 
- `neff::Number`: 
- `DeltaX::Number`: 
- `Deltay::Number`: 
- `DeltaZ::Number`: 
"""
function philp(
    phil::AbstractVector,
    nl::AbstractMatrix,
    nlp::AbstractMatrix,
    neff::Number,
    DeltaX::Number,
    DeltaY::Number,
    DeltaZ::Number,
)
    A = Amat(nl, nlp, neff, DeltaX, DeltaY, DeltaZ)
    C = Cmat(nl, nlp, neff, DeltaX, DeltaY, DeltaZ)
    A \ (C * phil)
end

"""
    philp(phil, nl, nlp, neff, DeltaX, Deltay, DeltaZ)

TODO: what is this

# Arguments
- `phil::AbstractVector`: 
- `nl::AbstractVector`: 
- `nlp::AbstractVector`: 
- `neff::Number`: 
- `DeltaX::Number`: 
- `Deltay::Number`: 
- `DeltaZ::Number`: 
"""
function philp(
    phil::AbstractVector,
    nl::AbstractVector,
    nlp::AbstractVector,
    neff::Number,
    DeltaX::Number,
    DeltaZ::Number,
)
    a = Amat(nl, nlp, neff, DeltaX, DeltaZ)
    c = Cmat(nl, nlp, neff, DeltaX, DeltaZ)
    a \ (c * phil)
end

"""
    bpm(n, phi1, DeltaX[, DeltaY], DeltaZ, neff)

FD-BPM propagation.

# Arguments
- `n`: refractive index.
- `phi1`: incident field.
- `DeltaX::Number`: unit-less sample spacing along x axis.
- `DeltaY::Number`: unit-less sample spacing along y axis.
- `DeltaZ::Number`: unit-less sample spacing along z axis.
- `neff::Number`: effective index for propagation.
"""
function bpm(
    n::AbstractArray,
    phi1::AbstractMatrix,
    DeltaX::Number,
    DeltaY::Number,
    DeltaZ::Number,
    neff::Number,
) where {F<:AbstractFloat}
    @assert ndims(n) == 3
    phi = Complex.(zeros(eltype(phi1), (size(n, 1) * size(n, 2), size(n, 3))))
    phi[:, 1] = phi1[:]
    for i = 2:size(n, 3)
        phi[:, i] =
            philp(phi[:, i-1], n[:, :, i-1], n[:, :, i], neff, DeltaX, DeltaY, DeltaZ)
    end
    reshape(phi, size(n))
end

function bpm(
    n::AbstractMatrix,
    phi1::AbstractVector,
    DeltaX::Number,
    DeltaZ::Number,
    neff::Number,
)
    phi = Complex.(zeros(eltype(phi1), size(n)))
    phi[:, 1] = phi1
    for i = 2:size(n, 2)
        phi[:, i] = philp(phi[:, i-1], n[:, i-1], n[:, i], neff, DeltaX, DeltaZ)
    end
    phi
end
