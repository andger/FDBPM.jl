import LinearAlgebra, SparseArrays

#CEM Lecture 17: BPM

"""
    speye(n)

Create identity matrix of size `(n,n)`.
"""
speye(n::Integer) = LinearAlgebra.Diagonal(ones(n))

#TODO: Remove unneeded terms
#TODO: Sanity check this
"""
    bpm(URxx, URyy, URzz, ERxx, ERyy, ERzz, Ey1, DeltaX, DeltaZ, neff)

FD-BPM propagation of 1D field in 2D.

# Arguments
- `URxx::AbstractMatrix`: xx-term of relative permeability.
- `URyy::AbstractMatrix`: yy-term of relative permeability.
- `URzz::AbstractMatrix`: zz-term of relative permeability.
- `ERxx::AbstractMatrix`: xx-term of relative permittivity.
- `ERyy::AbstractMatrix`: xx-term of relative permittivity.
- `ERzz::AbstractMatrix`: xx-term of relative permittivity.
- `Ey1::AbstractVector`: y-component of incident electric field.
- `DeltaX::Number`: unit-less sample spacing along x axis.
- `DeltaZ::Number`: unit-less sample spacing along z axis.
- `neff::Number`: effective index for propagation.
"""
function bpm(
    URxx::AbstractMatrix,
    URyy::AbstractMatrix,
    URzz::AbstractMatrix,
    ERxx::AbstractMatrix,
    ERyy::AbstractMatrix,
    ERzz::AbstractMatrix,
    Ey1::AbstractVector,
    DeltaX::Number,
    DeltaZ::Number,
    neff::Number,
)
    @warn "Possibly/Likely broken or incorrect"
    @assert size(URxx) == size(URyy) == size(URzz) == size(ERxx) == size(ERyy) == size(ERzz)
    @assert length(Ey1) == size(URxx)[1]
    Ey = complex.(zeros(size(URxx)))
    Ey[:, 1] = Ey1

    n = length(Ey1)
    DEX =
        DHX = LinearAlgebra.Tridiagonal(
            fill(-1 / (2 * DeltaX), n - 1),
            zeros(n),
            fill(1 / (2 * DeltaX), n - 1),
        )

    URxxi, URyyi, URzzi = LinearAlgebra.Diagonal.([URxx[:, 1], URyy[:, 1], URzz[:, 1]])
    ERxxi, ERyyi, ERzzi = LinearAlgebra.Diagonal.([ERxx[:, 1], ERyy[:, 1], ERzz[:, 1]])
    Ai = URxxi * DHX * (URzzi)^-1 * DEX + URxxi * ERyyi - neff^2 * speye(size(URxxi)[1])

    for i = 1:size(URxx)[2]-1
        URxxip, URyyip, URzzip =
            LinearAlgebra.Diagonal.([URxx[:, i+1], URyy[:, i+1], URzz[:, i+1]])
        ERxxip, ERyyip, ERzzip =
            LinearAlgebra.Diagonal.([ERxx[:, i+1], ERyy[:, i+1], ERzz[:, i+1]])
        Aip =
            URxxip * DHX * (URzzip)^-1 * DEX + URxxip * ERyyip -
            neff^2 * speye(size(URxxip)[1])

        Ey[:, i+1] =
            (speye(size(URxxip)[1]) - im * DeltaZ / (4 * neff) * Aip) \
            ((speye(size(URxxi)[1]) + im * DeltaZ / (4 * neff) * Ai) * Ey[:, i])

        URxxi, URyyi, URzzi = URxxip, URyyip, URzzip
        ERxxi, ERyyi, ERzzi = ERxxip, ERyyip, ERzzip
    end
    Ey
end

"""
    bpm(URxx, URyy, URzz, ERxx, ERyy, ERzz, Ex1, Ey1, DeltaX, DeltaZ, neff)

FD-BPM propagation of 2D field in 3D.

# Arguments
- `URxx::AbstractArray`: xx-term of relative permeability.
- `URyy::AbstractArray`: yy-term of relative permeability.
- `URzz::AbstractArray`: zz-term of relative permeability.
- `ERxx::AbstractArray`: xx-term of relative permittivity.
- `ERyy::AbstractArray`: xx-term of relative permittivity.
- `ERzz::AbstractArray`: xx-term of relative permittivity.
- `Ex1::AbstractMatrix`: x-component of incident electric field.
- `Ey1::AbstractMatrix`: y-component of incident electric field.
- `DeltaX::Number`: unit-less sample spacing along x (and y) axis.
- `DeltaZ::Number`: unit-less sample spacing along z axis.
- `neff::Number`: effective index for propagation.
"""
function bpm(
    URxx::AbstractArray,
    URyy::AbstractArray,
    URzz::AbstractArray,
    ERxx::AbstractArray,
    ERyy::AbstractArray,
    ERzz::AbstractArray,
    Ex1::AbstractMatrix,
    Ey1::AbstractMatrix,
    DeltaX::Number,
    DeltaZ::Number,
    neff::Number,
)
    @warn "Possibly/Likely broken or incorrect"

    Nx, Ny = size(Ey1)
    Nz = size(URxx, 3)
    Nmat = Nx * Ny

    Exy = complex.(zeros(Nmat * 2, Nz))
    Exy[1:Nmat, 1] .= Ex1[:]
    Exy[Nmat.+(1:Nmat), 1] .= Ey1[:]

    i = [1:Nmat; [x for x = 1:Nmat-1 if !iszero(x % Nx)]]
    j = [1:Nmat; [x for x = 2:Nmat if !iszero(x % Nx - 1)]]
    v = [fill(-1, Nmat); ones(Nmat - Ny)]
    DEX = DHX = SparseArrays.sparse(i, j, v)
    i = [1:Nmat; 1:Nmat-Ny]
    j = [1:Nmat; 1+Ny:Nmat]
    v = [fill(-1, Nmat); ones(Nmat - Ny)]
    DEY = DHY = SparseArrays.sparse(i, j, v)

    URxxi, URyyi, URzzi =
        LinearAlgebra.Diagonal.([URxx[:, :, 1][:], URyy[:, :, 1][:], URzz[:, :, 1][:]])
    ERxxi, ERyyi, ERzzi =
        LinearAlgebra.Diagonal.([ERxx[:, :, 1][:], ERyy[:, :, 1][:], ERzz[:, :, 1][:]])

    Pi = [
        -DEX * ERzzi^-1 * DHY URyyi + DEX * ERzzi^-1 * DHX
        -(URxxi + DEY * ERzzi^-1 * DHY) DEY * ERzzi^-1 * DHX
    ]
    Qi = [
        -DHX * URzzi^-1 * DEY ERyyi + DHX * URzzi^-1 * DEX
        -(ERxxi + DHY * URzzi^-1 * DEY) DHY * URzzi^-1 * DEX
    ]
    Ai = neff^2 * speye(2 * size(URxxi)[1]) + Pi * Qi
    #TODO: Refactor to use pre-allocated arrays, in-place updates

    for i = 1:size(URxx)[2]-1
        URxxip, URyyip, URzzip =
            LinearAlgebra.Diagonal.([
                URxx[:, :, i+1][:],
                URyy[:, :, i+1][:],
                URzz[:, :, i+1][:],
            ])
        ERxxip, ERyyip, ERzzip =
            LinearAlgebra.Diagonal.([
                ERxx[:, :, i+1][:],
                ERyy[:, :, i+1][:],
                ERzz[:, :, i+1][:],
            ])
        Pip = [
            -DEX * ERzzip^-1 * DHY URyyip + DEX * ERzzip^-1 * DHX
            -(URxxip + DEY * ERzzip^-1 * DHY) DEY * ERzzip^-1 * DHX
        ]
        Qip = [
            -DHX * URzzip^-1 * DEY ERyyip + DHX * URzzip^-1 * DEX
            -(ERxxip + DHY * URzzip^-1 * DEY) DHY * URzzip^-1 * DEX
        ]
        Aip = neff^2 * speye(2 * size(URxxip)[1]) + Pip * Qip

        Exy[:, i+1] =
            (speye(2 * size(URxxip)[1]) - DeltaZ / (4im * neff) * Aip) \
            ((speye(2 * size(URxxi)[1]) + DeltaZ / (4im * neff) * Ai) * Exy[:, i])

        URxxi, URyyi, URzzi = URxxip, URyyip, URzzip
        ERxxi, ERyyi, ERzzi = ERxxip, ERyyip, ERzzip
    end
    Ex = reshape(Exy[1:Nmat, :], (Nx, Ny, Nz))
    Ey = reshape(Exy[Nmat.+(1:Nmat), :], (Nx, Ny, Nz))
    Ex, Ey
end
