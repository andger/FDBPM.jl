using Documenter
import FDBPM

makedocs(
    sitename = "FDBPM.jl",
    pages = [
        "Home" => "index.md",
        "Examples" => [
            "Simple Usage" => "example.md",
            "Anisotropic Materials" => "example_vec.md",
            "3D BPM" => "example_3d.md",
        ],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["bpm.jl" => "bpm.md", "bpm_vec.jl" => "bpm_vec.md"],
        "test/" => ["bpm.jl" => "bpm_test.md", "bpm_vec.jl" => "bpm_vec_test.md"],
    ],
)
