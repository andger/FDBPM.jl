# 3D Usage

BPM propagation in 3D is just as simple as in 2D.
For scalar/isotropic medium problems we simply have `n` and `phi1`
gain an extra dimension, and we provide the sample spacing `DeltaY`
on the new axis.
For vectorial/anisotropic medium problems the permittivity and
permeability arrays `ERxx,ERyy,ERzz,URxx,URyy,URzz` similarly gain a
dimension. However, we now must provide two components of incident field
`Ex1` and `Ey1` (which are two dimensional arrays, and we will
obtain two field components after propagation), and the sample spacing
for the two transverse dimensions is assumed to be the same.
