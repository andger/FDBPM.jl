# Advanced Example

This package also supports vectorial fields in anisotropic media. These
also be propagated using the `bpm` function. We will repeat the simple
example using the vectorial solver:

```@example bpm2dvec
import Plots: plot, heatmap
import Plots: savefig # hide

DeltaX=0.05
DeltaZ=0.05
x=-6:DeltaX:6
z=0:DeltaZ:6
Nx, Nz=length.([x, z])
n=[x^2+(z-3)^2<=1 ? 3.0 : 1.0 for x=x, z=z]
phi1=[abs(x)<=1.5 ? 1.0 : 0.0 for x=x]

plot(
	heatmap(z, x, n,
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Index Structure",
	),
	plot(x, phi1,
		xlabel="x",
		ylabel="Field Amplitude",
		legend=false,
	)
)
savefig("problemvec.png"); nothing # hide
```

![Refractive index structure and initial field](problemvec.png)

We now provide an effective (propagation) index `neff`
and propagate the field. We then plot the results:

```@example bpm2dvec
import FDBPM

neff=maximum(n)
erxx=eryy=erzz=n.^2
urxx=uryy=urzz=ones(size(n))
phi=FDBPM.bpm(erxx, eryy, erzz, urxx, uryy, urzz, phi1, DeltaX, DeltaZ, neff)

plot(
	heatmap(z, x, abs.(phi),
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Magnitude",
	),
	heatmap(z, x, angle.(phi),
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Phase",
	)
)
savefig("propfieldvec.png"); nothing # hide
```

![Results of BPM](propfieldvec.png)

In order to obtain the complex propagated field we must add in the phase
of the propagated field (the results plotted above are only the magnitude
and phase relative to a propagating plane wave):

```@example bpm2dvec
phase=ones(Nx)*exp.(im*z.*neff)'
phi.*=phase

plot(
	heatmap(z, x, abs.(phi),
		xlabel="x",
		ylabel="z",
		aspectratio=1,
		title="Magnitude",
	),
	heatmap(z, x, angle.(phi),
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Phase",
	)
)
savefig("propfield_corrvec.png"); nothing # hide
```

![Propagated field](propfield_corrvec.png)
