# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some potential areas for development and improvement:

* More, better unit tests and documentation (including of 3D problems)
* Optimizations (especially in terms of allocations)
* Implement boundary conditions
* Non-uniformly sampled grids?
* Implement mode-solving and similar functionality using BPM
* Verify correctness (examples and references)
* Determine, document, and ideally unify conventions for the various implementations

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support both 2D and 3D problems
* We aim to minimize the amount of code and function repetition when implementing the above features
