# Advanced Beam Propagation Method

Here we implement more advanced FD-BPM algorithms for anisotropic media.

```@autodocs
Modules = [FDBPM]
Pages   = ["bpm_vec.jl"]
```
